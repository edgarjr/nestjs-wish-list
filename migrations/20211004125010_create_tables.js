exports.up = function (knex) {
  return knex.schema
    .createTable('user', function (table) {
      table.increments('id');
      table.string('username', 255).unique().notNullable();
      table.string('password_hash', 255).notNullable();
      table.string('current_token', 1000).notNullable();
    })
    .createTable('category', function (table) {
      table.increments('id');
      table.string('description', 255).notNullable();
    })
    .createTable('wish', function (table) {
      table.increments('id');
      table.string('title', 255).notNullable();
      table.string('description', 255).notNullable();
      table.string('budget', 255);
      table.timestamp('due_date');
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.integer('user_id').unsigned().references('id').inTable('user');
      table
        .integer('category_id')
        .unsigned()
        .references('id')
        .inTable('category');
    })
    .createTable('item', function (table) {
      table.increments('id');
      table.string('link', 1000).notNullable();
      table.decimal('price').notNullable();
      table.string('description', 1000);
      table.integer('rate');
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.integer('wish_id').unsigned().references('id').inTable('wish');
    });
};

exports.down = function (knex) {
  return knex.schema
    .dropTable('item')
    .dropTable('wish')
    .dropTable('category')
    .dropTable('user');
};
