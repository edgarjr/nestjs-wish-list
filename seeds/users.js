exports.seed = function (knex) {
  return knex('user')
    .del()
    .then(function () {
      return knex('user').insert([
        {
          username: 'carlos',
          password_hash:
            '$2a$10$cd8WOZg.XRh8yQthOKswKeNMNwadBdFsKM.Jf/cPVOwcaJ9fmt2ea',
          current_token: '231313123123123',
        },
        {
          username: 'antonio',
          password_hash:
            '$2a$10$cd8WOZg.XRh8yQthOKswKeNMNwadBdFsKM.Jf/cPVOwcaJ9fmt2ea',
          current_token: '231313123123123',
        },
        {
          username: 'miguel',
          password_hash:
            '$2a$10$cd8WOZg.XRh8yQthOKswKeNMNwadBdFsKM.Jf/cPVOwcaJ9fmt2ea',
          current_token: '231313123123123',
        },
      ]);
    });
};
