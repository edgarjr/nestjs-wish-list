import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { DatabaseConfigService } from './database.config.service';
@Module({
  imports: [ConfigService],
  providers: [DatabaseConfigService],
  exports: [DatabaseConfigService],
})
export class AppConfigModule {}
