import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';

@Injectable()
export class DatabaseConfigService {
  constructor(private configService: ConfigService) {}

  get host(): string {
    console.log('host', this.configService.get<string>('DB_HOST'));
    return this.configService.get<string>('DB_HOST');
  }
  get user(): string {
    return this.configService.get<string>('DB_USER');
  }
  get password(): string {
    return this.configService.get<string>('DB_PASSWORD');
  }
  get database(): string {
    return this.configService.get<string>('DB_DATABASE');
  }
}
