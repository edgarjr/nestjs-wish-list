import { AppConfigModule } from './config/app.config.module';
import { ItemsModule } from './items/items.module';
import { WishesModule } from './wishes/wishes.module';
import { AuthModule } from './auth/auth.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { KnexModule } from 'nest-knexjs';
import { UsersModule } from './users/users.module';
import { ConfigModule } from '@nestjs/config';
import { DatabaseConfigService } from './config/database.config.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    AuthModule,
    AppConfigModule,
    KnexModule.forRootAsync({
      imports: [AppConfigModule],
      useFactory: async (config: DatabaseConfigService) => ({
        config: {
          client: 'mysql',
          connection: {
            host: config.host,
            user: config.user,
            password: config.password,
            database: config.database,
          },
        },
      }),
      inject: [DatabaseConfigService],
    }),
    UsersModule,
    WishesModule,
    ItemsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
