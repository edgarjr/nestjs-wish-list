import { ApiProperty } from '@nestjs/swagger';

export class BaseEntity<T> {
  @ApiProperty()
  id?: T;
}
