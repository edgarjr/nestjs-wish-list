import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from './base.entity';

export class Wish extends BaseEntity<number> {
  @ApiProperty()
  user_id: number;
  @ApiProperty()
  category_id: number;
  @ApiProperty()
  title: string;
  @ApiProperty()
  description?: string;
  @ApiProperty()
  budget?: string;
  @ApiProperty()
  due_date?: Date;
  @ApiProperty()
  created_at?: Date;

  constructor(partial?: Partial<Wish>) {
    super();
    Object.assign(this, partial);
  }
}
