import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from './base.entity';

export class Item extends BaseEntity<number> {
  @ApiProperty()
  wish_id: number;
  @ApiProperty()
  link: string;
  @ApiProperty()
  price: number;
  @ApiProperty()
  description: string;
  @ApiProperty()
  rate: number;
  @ApiProperty()
  created_at: Date;

  constructor(partial?: Partial<Item>) {
    super();
    Object.assign(this, partial);
  }
}
