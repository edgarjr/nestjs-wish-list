import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from './base.entity';

export class User extends BaseEntity<number> {
  @ApiProperty()
  username: string;
  @ApiProperty()
  password_hash?: string;
  @ApiProperty()
  current_token?: string;

  constructor(partial?: Partial<User>) {
    super();
    Object.assign(this, partial);
  }
}
