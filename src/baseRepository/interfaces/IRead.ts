export interface IRead<T> {
  findAll(item: T): Promise<T[]>;
  findOne(id: number): Promise<T>;
}
