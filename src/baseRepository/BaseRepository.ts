import { IWrite } from './interfaces/IWrite';
import { IRead } from './interfaces/IRead';
import { Knex } from 'knex';

export abstract class BaseRepository<T> implements IWrite<T>, IRead<T> {
  constructor(
    private readonly tableName: string,
    private readonly knex: Knex,
  ) {}

  async findOne(id: number): Promise<T> {
    const user = await this.knex.table(this.tableName).where('id', id).first();
    return user;
  }

  async remove(id: number) {
    return await this.knex.table(this.tableName).where('id', id).del();
  }

  async create(item: T): Promise<boolean> {
    await this.knex.table(this.tableName).insert<T>(item);
    return true;
  }

  async update(id: number, item: T): Promise<boolean> {
    await this.knex.table(this.tableName).where('id', id).update<T>(item);
    return true;
  }

  async delete(id: number): Promise<number> {
    return await this.knex.table(this.tableName).where('id', id).del();
  }
  async findAll(): Promise<T[]> {
    const items = await this.knex.table(this.tableName).select();
    return items;
  }
}
