import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { AuthLoginDto } from './dto/auth-login.dto';

const accessToken = {
  access_token: '1234567890',
};

describe('UsersController', () => {
  let authController: AuthController;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: {
            login: jest.fn().mockResolvedValue(accessToken),
          },
        },
      ],
    }).compile();

    authController = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(authController).toBeDefined();
    expect(authService).toBeDefined();
  });

  describe('login', () => {
    it('should return a valid access token', async () => {
      const body: AuthLoginDto = {
        username: 'carlos',
        password: '12345',
      };

      const result = await authController.login(body);

      expect(result).toEqual(accessToken);
      expect(authService.login).toHaveBeenCalledTimes(1);
    });

    it('should throw an exception', () => {
      jest.spyOn(authService, 'login').mockRejectedValueOnce(new Error());
      expect(authController.login).rejects.toThrowError();
    });
  });
});
