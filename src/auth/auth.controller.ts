import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import { AuthService } from './auth.service';
import { AuthLoginDto } from './dto/auth-login.dto';
import { JwtAuthGuard } from './jwt/jwt-auth.guard';

@Controller('api/auth')
@ApiTags('authentication')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post()
  @ApiOperation({ summary: 'Login' })
  @ApiResponse({ status: 200, description: 'Ok' })
  @ApiResponse({
    status: 401,
    description:
      'Unauthorized. User is not authenticated at all, token is expired or token is invalid.',
  })
  @ApiResponse({
    status: 500,
    description:
      'InternalServerError. Something unexpected went wrong while authentication',
  })
  async login(@Body() authLoginDto: AuthLoginDto) {
    return this.authService.login(authLoginDto);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Get('/test')
  @ApiOperation({ summary: 'Test Authentication' })
  @ApiResponse({ status: 200, description: 'Success' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  async test() {
    return 'Success!';
  }
}
