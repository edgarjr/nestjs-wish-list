import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { ItemsRepository } from './ItemsRepository';
import { Item } from '../entities/item.entity';
import { CreateItemDto } from 'src/items/dto/create-item.dto';
import { UpdateItemDto } from './dto/update-item.dto';

@Injectable()
export class ItemsService {
  constructor(private readonly repository: ItemsRepository) {}

  async findAll(): Promise<Item[]> {
    return await this.repository.findAll();
  }

  async create(createItemDto: CreateItemDto) {
    try {
      const itemToSave = new Item(createItemDto);
      return await this.repository.create(itemToSave);
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }

  async findOne(id: number): Promise<Item> {
    if (!id) {
      throw new BadRequestException(`Item ${id} is invalid`);
    }

    const item = await this.repository.findOne(id);
    if (item) {
      return item;
    } else {
      throw new NotFoundException(`Item ${id} was not found`);
    }
  }

  async search(wishId?: number): Promise<Item[]> {
    return this.repository.search(wishId);
  }

  async update(id: number, updateItemDto: UpdateItemDto): Promise<boolean> {
    try {
      const itemToUpdate = new Item(updateItemDto);
      return await this.repository.update(id, itemToUpdate);
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  async remove(id: number): Promise<boolean> {
    if (!id) {
      throw new BadRequestException(`Item ${id} is invalid`);
    }

    const result = await this.repository.remove(id);
    if (result) {
      return true;
    } else {
      throw new NotFoundException(`Item ${id} was not found`);
    }
  }
}
