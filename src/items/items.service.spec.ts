import { Test, TestingModule } from '@nestjs/testing';
import { ItemsService } from './items.service';
import { ItemsRepository } from './ItemsRepository';
import { Item } from '../entities/item.entity';
import { CreateItemDto } from 'src/items/dto/create-item.dto';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { UpdateItemDto } from './dto/update-item.dto';

describe('ItemsService', () => {
  let itemsService: ItemsService;
  let itemRepository: ItemsRepository;

  const items: Item[] = [
    new Item({ id: 1, wish_id: 1 }),
    new Item({ id: 2, wish_id: 1 }),
    new Item({ id: 3, wish_id: 1 }),
  ];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ItemsService,
        {
          provide: ItemsRepository,
          useValue: {
            findOne: jest.fn().mockResolvedValue(items[0]),
            findAll: jest.fn().mockResolvedValue(items),
            create: jest.fn().mockResolvedValue(true),
            update: jest.fn().mockResolvedValue(true),
            search: jest.fn().mockResolvedValue(items),
            remove: jest.fn().mockResolvedValue(true),
          },
        },
      ],
    }).compile();

    itemsService = module.get<ItemsService>(ItemsService);
    itemRepository = module.get<ItemsRepository>(ItemsRepository);
  });

  it('should be defined', () => {
    expect(itemsService).toBeDefined();
    expect(itemRepository).toBeDefined();
  });

  describe('findOne', () => {
    it('should return a item succesfully', async () => {
      const result = await itemsService.findOne(1);
      expect(result).toEqual(items[0]);
      expect(itemRepository.findOne).toHaveBeenCalledTimes(1);
    });

    it('should return a BadRequestException whether id is missing', async () => {
      try {
        await itemsService.findOne(undefined);
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error).toHaveProperty('message', 'Item undefined is invalid');
      }
    });

    it('should throw an NotFound exception whether id is invalid ', async () => {
      jest.spyOn(itemRepository, 'findOne').mockResolvedValue(undefined);

      try {
        await itemsService.findOne(1);
      } catch (error) {
        expect(error).toBeInstanceOf(NotFoundException);
        expect(error).toHaveProperty('message', 'Item 1 was not found');
      }
    });

    it('should throw an exception', () => {
      jest.spyOn(itemRepository, 'findOne').mockRejectedValueOnce(new Error());
      expect(itemsService.findOne(1)).rejects.toThrowError();
    });
  });

  describe('findAll', () => {
    it('should return a list of items succesfully', async () => {
      const result = await itemsService.findAll();
      expect(result).toEqual(items);
      expect(itemRepository.findAll).toHaveBeenCalledTimes(1);
    });

    it('should throw an exception', () => {
      jest.spyOn(itemRepository, 'findAll').mockRejectedValueOnce(new Error());
      expect(itemsService.findAll()).rejects.toThrowError();
    });
  });

  describe('create', () => {
    const body: CreateItemDto = {
      wish_id: 1,
      description: 'item description',
      link: '',
      price: 0,
      rate: 0,
    };

    it('should create a new wish item succesfully', async () => {
      const result = await itemsService.create(body);
      expect(result).toEqual(true);
      expect(itemRepository.create).toHaveBeenCalledTimes(1);
    });

    it('should throw an exception', () => {
      jest.spyOn(itemRepository, 'create').mockRejectedValueOnce(new Error());
      expect(itemsService.create(body)).rejects.toThrowError();
    });
  });

  describe('update', () => {
    const body: UpdateItemDto = {
      wish_id: 1,
      description: 'item description',
      link: '',
      price: 0,
      rate: 0,
    };

    it('should update a wish item succesfully', async () => {
      const result = await itemsService.update(1, body);
      expect(result).toEqual(true);
      expect(itemRepository.update).toHaveBeenCalledTimes(1);
    });

    it('should throw an exception', () => {
      jest.spyOn(itemRepository, 'update').mockRejectedValueOnce(new Error());
      expect(itemsService.update(1, body)).rejects.toThrowError();
    });
  });

  describe('remove', () => {
    it('should remove a item succesfully', async () => {
      const result = await itemsService.remove(1);
      expect(result).toEqual(true);
      expect(itemRepository.remove).toHaveBeenCalledTimes(1);
    });

    it('should return a BadRequestException whether id is missing', async () => {
      try {
        await itemsService.remove(undefined);
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
        expect(error).toHaveProperty('message', 'Item undefined is invalid');
      }
    });

    it('should throw an NotFound exception whether id is invalid ', async () => {
      jest.spyOn(itemRepository, 'remove').mockResolvedValue(undefined);
      try {
        await itemsService.remove(1);
      } catch (error) {
        expect(error).toBeInstanceOf(NotFoundException);
        expect(error).toHaveProperty('message', 'Item 1 was not found');
      }
    });

    it('should throw an exception', () => {
      jest.spyOn(itemRepository, 'remove').mockRejectedValueOnce(new Error());
      expect(itemsService.remove(1)).rejects.toThrowError();
    });
  });
});
