import { IsNotEmpty, IsNumberString } from 'class-validator';

export class DeleteItemDto {
  @IsNotEmpty()
  @IsNumberString()
  wish_id: number;

  @IsNotEmpty()
  @IsNumberString()
  item_id: number;
}
