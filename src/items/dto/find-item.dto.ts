import { IsNotEmpty, IsNumberString } from 'class-validator';

export class FindItemDto {
  @IsNotEmpty()
  @IsNumberString()
  wish_id: number;

  @IsNotEmpty()
  @IsNumberString()
  item_id: number;
}
