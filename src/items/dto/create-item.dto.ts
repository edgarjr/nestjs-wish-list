import {
  IsNotEmpty,
  IsString,
  IsInt,
  IsCurrency,
  IsOptional,
} from 'class-validator';

export class CreateItemDto {
  @IsNotEmpty()
  @IsInt()
  wish_id: number;

  @IsString()
  description: string;

  @IsNotEmpty()
  @IsString()
  link: string;

  @IsNotEmpty()
  @IsCurrency()
  price: number;

  @IsOptional()
  @IsInt()
  rate?: number;
}
