import { Injectable } from '@nestjs/common';
import { Knex } from 'knex';
import { InjectModel } from 'nest-knexjs';
import { Item } from '../entities/item.entity';
import { BaseRepository } from '../baseRepository/BaseRepository';

@Injectable()
export class ItemsRepository extends BaseRepository<Item> {
  private readonly _knex: Knex;
  private readonly _tableName: string = 'item';

  constructor(@InjectModel() knex: Knex) {
    super('item', knex);
    this._knex = knex;
  }

  async search(wishId?: number): Promise<Item[]> {
    const items = await this._knex
      .table(this._tableName)
      .modify(function (queryBuilder) {
        if (wishId) {
          queryBuilder.where('wish_id', wishId);
        }
      });

    return items;
  }
}
