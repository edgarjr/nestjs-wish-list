import { Module } from '@nestjs/common';
import { ItemsRepository } from 'src/items/ItemsRepository';
import { ItemsService } from './items.service';

@Module({
  imports: [],
  providers: [ItemsService, ItemsRepository],
  exports: [ItemsService, ItemsRepository],
})
export class ItemsModule {}
