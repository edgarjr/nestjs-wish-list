import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from '../entities/user.entity';

const usersList: any[] = [
  { id: 1, username: 'antonio' },
  { id: 2, username: 'joao' },
  { id: 3, username: 'carlos' },
];

const user: User = {
  id: 1,
  username: 'antonio',
};

describe('UsersController', () => {
  let usersController: UsersController;
  let usersService: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useValue: {
            findAll: jest.fn().mockResolvedValue(usersList),
            findOne: jest.fn().mockResolvedValue(user),
            create: jest.fn().mockResolvedValue(1),
            update: jest.fn().mockResolvedValue(user),
            remove: jest.fn().mockResolvedValue(user.id),
          },
        },
      ],
    }).compile();

    usersController = module.get<UsersController>(UsersController);
    usersService = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(usersController).toBeDefined();
    expect(usersService).toBeDefined();
  });

  describe('findAll', () => {
    it('should return a user list succesfully', async () => {
      const result = await usersController.findAll();
      expect(result).toEqual(usersList);
      expect(typeof result).toEqual('object');
      expect(usersService.findAll).toHaveBeenCalledTimes(1);
    });

    it('should throw an exception', () => {
      jest.spyOn(usersService, 'findAll').mockRejectedValueOnce(new Error());
      expect(usersController.findAll()).rejects.toThrowError();
    });
  });

  describe('findOne', () => {
    it('should return a user succesfully', async () => {
      const result = await usersController.findOne('1');
      expect(result).toEqual(user);
      expect(usersService.findOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an exception', () => {
      jest.spyOn(usersService, 'findOne').mockRejectedValueOnce(new Error());
      expect(usersController.findOne('1')).rejects.toThrowError();
    });
  });

  describe('create', () => {
    it('should add a user succesfully', async () => {
      const body: CreateUserDto = {
        username: 'carlos',
        password: '12345',
      };

      const result = await usersController.create(body);
      expect(result).toEqual(1);
      expect(usersService.create).toHaveBeenCalledTimes(1);
    });

    it('should throw an exception', () => {
      const body: CreateUserDto = {
        username: 'carlos',
        password: '12345',
      };

      jest.spyOn(usersService, 'create').mockRejectedValueOnce(new Error());
      expect(usersController.create(body)).rejects.toThrowError();
    });
  });

  describe('update', () => {
    it('should update a user succesfully', async () => {
      const body: UpdateUserDto = {
        username: 'antonio',
        password: '12345',
      };

      await usersController.update(1, body);
      expect(usersService.update).toHaveBeenCalledTimes(1);
    });

    it('should throw an exception', () => {
      const body: UpdateUserDto = {
        username: 'antonio',
        password: '12345',
      };

      jest.spyOn(usersService, 'update').mockRejectedValueOnce(new Error());
      expect(usersController.update(1, body)).rejects.toThrowError();

      jest.spyOn(usersService, 'update').mockRejectedValueOnce(new Error());
      expect(usersController.update(1, body)).rejects.toThrowError();
    });
  });

  describe('remove', () => {
    it('should delete a user succesfully', async () => {
      const result = await usersController.remove(user.id.toString());
      expect(result).toEqual(user.id);
      expect(usersService.remove).toHaveBeenCalledTimes(1);
    });

    it('should throw an exception', () => {
      jest.spyOn(usersService, 'remove').mockRejectedValueOnce(new Error());
      expect(usersController.remove(user.id.toString())).rejects.toThrowError();
    });
  });
});
