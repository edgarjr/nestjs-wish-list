import { Injectable } from '@nestjs/common';
import { Knex } from 'knex';
import { InjectModel } from 'nest-knexjs';
import { User } from '../entities/user.entity';
import { BaseRepository } from '../baseRepository/BaseRepository';

@Injectable()
export class UsersRepository extends BaseRepository<User> {
  private readonly _knex: Knex;
  private readonly _tableName: string = 'user';

  constructor(@InjectModel() knex: Knex) {
    super('user', knex);
    this._knex = knex;
  }

  async findByUsername(username: string): Promise<User> {
    return await this._knex
      .table(this._tableName)
      .where<User>('username', username)
      .first();
  }
}
