import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseGuards,
  UsePipes,
  ValidationPipe,
  HttpCode,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { JwtAuthGuard } from '../auth/jwt/jwt-auth.guard';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ListUserSwagger } from './swagger/list-user.swagger';
import { ShowUserSwagger } from './swagger/show-user.swagger';
import { User } from '../entities/user.entity';

@Controller('/api/users')
@ApiTags('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'List of all users' })
  @ApiResponse({
    status: 200,
    description: 'Ok',
    type: ListUserSwagger,
    isArray: true,
  })
  @ApiResponse({
    status: 401,
    description:
      'Unauthorized. User is not authenticated at all, token is expired or token is invalid.',
  })
  @ApiResponse({
    status: 500,
    description:
      'InternalServerError. Something unexpected went wrong while fetching the list of users.',
  })
  @Get()
  @ApiBearerAuth()
  async findAll() {
    const users: User[] = await this.usersService.findAll();

    return users.map((user) => {
      return { id: user.id, username: user.username };
    });
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  @ApiOperation({ summary: 'Gets detailed information about the user' })
  @ApiResponse({ status: 200, description: 'Ok', type: ShowUserSwagger })
  @ApiResponse({ status: 400, description: 'Invalid parameters' })
  @ApiResponse({
    status: 401,
    description:
      'Unauthorized. User is not authenticated at all, token is expired or token is invalid.',
  })
  @ApiResponse({ status: 404, description: 'Not found' })
  @ApiResponse({
    status: 500,
    description:
      'InternalServerError. Something unexpected went wrong while fetching the user.',
  })
  @ApiBearerAuth()
  async findOne(@Param('id') id: string) {
    const user = await this.usersService.findOne(+id);
    return { id: user.id, username: user.username };
  }

  @UseGuards(JwtAuthGuard)
  @UsePipes(ValidationPipe)
  @Post()
  @HttpCode(201)
  @ApiOperation({ summary: 'Create a user' })
  @ApiResponse({ status: 201, description: 'User created' })
  @ApiResponse({
    status: 400,
    description:
      'There was a problem with the parameters. Check the message body for further help.',
  })
  @ApiResponse({
    status: 401,
    description:
      'Unauthorized. User is not authenticated at all, token is expired or token is invalid.',
  })
  @ApiResponse({
    status: 500,
    description:
      'InternalServerError. Something unexpected went wrong while fetching the user.',
  })
  @ApiBearerAuth()
  async create(@Body() createUserDto: CreateUserDto) {
    return await this.usersService.create(createUserDto);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  @UsePipes(ValidationPipe)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Update a user' })
  @ApiResponse({ status: 200, description: 'User updated' })
  @ApiResponse({
    status: 400,
    description:
      'There was a problem with the parameters. Check the message body for further help.',
  })
  @ApiResponse({
    status: 401,
    description:
      'Unauthorized. User is not authenticated at all, token is expired or token is invalid.',
  })
  @ApiResponse({
    status: 404,
    description: 'Specified user id was not found to be updated',
  })
  @ApiResponse({
    status: 500,
    description:
      'InternalServerError. Something unexpected went wrong while editing the user.',
  })
  async update(@Param('id') id: number, @Body() updateUserDto: UpdateUserDto) {
    await this.usersService.update(id, updateUserDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Delete a user' })
  @ApiResponse({ status: 200, description: 'User was deleted successfully' })
  @ApiResponse({
    status: 401,
    description:
      'Unauthorized. User is not authenticated at all, token is expired or token is invalid.',
  })
  @ApiResponse({
    status: 404,
    description: 'Specified user id was not found to be deleted',
  })
  @ApiResponse({
    status: 500,
    description:
      'InternalServerError. Something unexpected went wrong while deleting the user.',
  })
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }
}
