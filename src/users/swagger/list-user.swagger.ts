import { OmitType } from '@nestjs/swagger';
import { User } from '../../entities/user.entity';

export class ListUserSwagger extends OmitType(User, [
  'password_hash',
  'current_token',
]) {}
