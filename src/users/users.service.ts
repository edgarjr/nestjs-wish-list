import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import * as bcrypt from 'bcryptjs';
import { User } from '../entities/user.entity';
import { UsersRepository } from './UsersRepository';

@Injectable()
export class UsersService {
  constructor(private readonly repository: UsersRepository) {}

  async findAll(): Promise<User[]> {
    return await this.repository.findAll();
  }

  async create(createUserDto: CreateUserDto): Promise<boolean> {
    try {
      const user = await this.repository.findByUsername(createUserDto.username);
      if (user) {
        throw new HttpException(
          `Username ${createUserDto.username} already exists`,
          HttpStatus.BAD_REQUEST,
        );
      }

      const salt = await bcrypt.genSalt(10);
      const hash = await bcrypt.hash(createUserDto.password, salt);

      const newUser: User = {
        username: createUserDto.username,
        password_hash: hash,
        current_token: '',
      };

      return await this.repository.create(newUser);
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }

  async findOne(id: number): Promise<User> {
    if (!id) {
      throw new BadRequestException(`User ${id} is invalid`);
    }

    const user = await this.repository.findOne(id);
    if (user) {
      return user;
    } else {
      throw new NotFoundException(`User ${id} has not found`);
    }
  }

  async findByUsername(username: string): Promise<User> {
    if (!username) {
      throw new BadRequestException(`User '${username}' is invalid`);
    }

    const user = await this.repository.findByUsername(username);
    if (user) {
      return user;
    } else {
      throw new NotFoundException(`User ${username} has not found`);
    }
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.repository.findOne(id);
    if (!user) {
      throw new NotFoundException(`User ${id} has not found`);
    }

    try {
      const user: User = {} as User;

      if (updateUserDto.username) {
        user.username = updateUserDto.username;
      }
      if (updateUserDto.currentToken) {
        user.current_token = updateUserDto.currentToken;
      }
      if (updateUserDto.password) {
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(updateUserDto.password, salt);
        user.password_hash = hash;
      }
      return await this.repository.update(id, user);
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  async remove(id: number) {
    if (!id) {
      throw new BadRequestException(`User ${id} is invalid`);
    }
    return await this.repository.remove(id);
  }
}
