import { Type } from 'class-transformer';
import {
  IsNotEmpty,
  IsString,
  IsInt,
  IsDate,
  IsOptional,
} from 'class-validator';

export class CreateWishDto {
  @IsNotEmpty()
  @IsInt()
  user_id: number;

  @IsNotEmpty()
  @IsInt()
  category_id: number;

  @IsNotEmpty()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  description?: string;

  @IsOptional()
  @IsString()
  budget?: string;

  @IsOptional()
  @Type(() => Date)
  @IsDate()
  due_date?: Date;

  @IsOptional()
  @Type(() => Date)
  @IsDate()
  created_at?: Date;
}
