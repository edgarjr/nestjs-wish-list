import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import { JwtAuthGuard } from '../auth/jwt/jwt-auth.guard';
import { Wish } from '../entities/wish.entity';
import { CreateItemDto } from 'src/items/dto/create-item.dto';
import { CreateWishDto } from './dto/create-wish.dto';
import { UpdateWishDto } from './dto/update-wish.dto';
import { WishesService } from './wishes.service';
import { ItemsService } from '../items/items.service';
import { DeleteItemDto } from '../items/dto/delete-item.dto';
import { FindItemDto } from '../items/dto/find-item.dto';

@Controller('api/wishes')
@ApiTags('wishes')
export class WishesController {
  constructor(
    private readonly wishesService: WishesService,
    private readonly itemsService: ItemsService,
  ) {}

  @Get()
  @ApiOperation({ summary: 'List all wishes' })
  @ApiResponse({
    status: 200,
    description: 'List of wishes',
    type: Wish,
    isArray: true,
  })
  async findAll() {
    return await this.wishesService.findAll();
  }

  @Get(':id')
  @ApiOperation({ summary: 'List all wishes' })
  @ApiResponse({
    status: 200,
    description: 'Get a wish',
    type: Wish,
  })
  async findOne(
    @Param(
      'id',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.BAD_REQUEST }),
    )
    id: number,
  ) {
    return await this.wishesService.findOne(id);
  }

  @Get(':id/items')
  async findItems(
    @Param(
      'id',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.BAD_REQUEST }),
    )
    id: number,
  ) {
    return await this.itemsService.search(id);
  }

  @Get(':wish_id/items/:item_id')
  async findItem(@Param() params: FindItemDto) {
    return await this.itemsService.findOne(params.item_id);
  }

  @UseGuards(JwtAuthGuard)
  @UsePipes(ValidationPipe)
  @ApiBearerAuth()
  @Post(':id/items')
  async createItems(
    @Body() createItemDto: CreateItemDto[],
    @Param(
      'id',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.BAD_REQUEST }),
    )
    id: number,
    @Req() req,
  ): Promise<void> {
    await this.wishesService.validateCreateItems(id, req?.user?.id);

    for (const item of createItemDto) {
      this.itemsService.create(item);
    }
  }

  @UseGuards(JwtAuthGuard)
  @UsePipes(ValidationPipe)
  @ApiBearerAuth()
  @Post()
  create(@Body() createWishDto: CreateWishDto, @Req() req) {
    this.wishesService.validateCreateWish(createWishDto.user_id, req);

    return this.wishesService.create(createWishDto);
  }

  @UseGuards(JwtAuthGuard)
  @UsePipes(ValidationPipe)
  @ApiBearerAuth()
  @Put()
  update(
    @Body() updateWishDto: UpdateWishDto,
    @Param(
      'id',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.BAD_REQUEST }),
    )
    id: number,
  ) {
    return this.wishesService.update(id, updateWishDto);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Delete(':id')
  remove(
    @Param(
      'id',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.BAD_REQUEST }),
    )
    id: number,
  ) {
    return this.wishesService.remove(+id);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Delete(':wish_id/items/:item_id')
  async removeItem(@Param() params: DeleteItemDto) {
    return await this.itemsService.remove(params.item_id);
  }
}
