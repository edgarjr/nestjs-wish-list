import { Injectable } from '@nestjs/common';
import { Knex } from 'knex';
import { InjectModel } from 'nest-knexjs';
import { Wish } from '../entities/wish.entity';
import { BaseRepository } from '../baseRepository/BaseRepository';

@Injectable()
export class WishesRepository extends BaseRepository<Wish> {
  private readonly _knex: Knex;
  private readonly _tableName: string = 'wish';

  constructor(@InjectModel() knex: Knex) {
    super('wish', knex);
    this._knex = knex;
  }

  async findByTitle(title: string): Promise<Wish[]> {
    const wishes = await this._knex
      .table('wish')
      .where('title', 'like', `%${title}%`);
    return wishes;
  }

  async search(userId?: number): Promise<Wish[]> {
    const wishes = await this._knex
      .table('wish')
      .modify(function (queryBuilder) {
        if (userId) {
          queryBuilder.where('user_id', userId);
        }
      });
    return wishes;
  }
}
