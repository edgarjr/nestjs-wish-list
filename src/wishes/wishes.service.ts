import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Wish } from '../entities/wish.entity';
import { WishesRepository } from './WishesRepository';

@Injectable()
export class WishesService {
  constructor(private readonly repository: WishesRepository) {}

  async findAll(): Promise<Wish[]> {
    return await this.repository.findAll();
  }

  async create(wish: Wish) {
    try {
      return await this.repository.create(wish);
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  async findOne(id: number): Promise<Wish> {
    if (!id) {
      throw new BadRequestException(`Wish ${id} is invalid`);
    }

    const wish = await this.repository.findOne(id);
    if (wish) {
      return wish;
    } else {
      throw new NotFoundException(`Wish ${id} has not found`);
    }
  }

  async search(wishId?: number): Promise<Wish[]> {
    return this.repository.search(wishId);
  }

  async update(id: number, item: Wish) {
    try {
      return await this.repository.update(id, item);
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  async remove(id: number) {
    if (!id) {
      throw new BadRequestException(`Wish ${id} is invalid`);
    }
    return await this.repository.remove(id);
  }

  async findByTitle(title: string): Promise<Wish[]> {
    if (!title) {
      throw new BadRequestException(`Wish '${title}' is invalid`);
    }
    return this.repository.findByTitle(title);
  }

  validateCreateWish(wishUserId: number, userId: number) {
    if (wishUserId != userId) {
      throw new ForbiddenException(
        'Invalid user. You have not permission to create a wish list to the given user',
      );
    }
  }

  async validateCreateItems(wishId: number, userId: number) {
    const wish = await this.repository.findOne(wishId);
    if (!wish) {
      throw new NotFoundException(`Wish ${wishId} was not found`);
    }

    if (wish.user_id != userId) {
      throw new ForbiddenException(
        'You have not permission to add items to this wish list',
      );
    }
  }
}
