import { WishesService } from './wishes.service';
import { WishesController } from './wishes.controller';
import { Module } from '@nestjs/common';
import { WishesRepository } from './WishesRepository';
import { ItemsModule } from './../items/items.module';

@Module({
  imports: [ItemsModule],
  controllers: [WishesController],
  providers: [WishesService, WishesRepository],
  exports: [WishesService, WishesRepository],
})
export class WishesModule {}
